# -*- coding: utf-8 -*-
'''
Created on 2 May 2017

@author: Guangyu Wu
'''


def hello_world():
    print('Hello World!')


def hello(name):
    print('Hello {}'.format(name))
