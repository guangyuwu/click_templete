# -*- coding: utf-8 -*-
'''
Created on 2 May 2017

@author: Guangyu Wu
'''

import click
from src.main import hello_world, hello


@click.command()
def do_hello_world():
    hello_world()


@click.command()
@click.argument('name')
def do_hello(name):
    hello(name)


@click.command()
@click.option('--count', default=1, help='Number of greetings.')
@click.option('--name', prompt='Your name', help='The person to greet.')
def do_hello_2(count, name):
    """Simple program that greets NAME for a total of COUNT times."""
    for _ in range(count):
        hello(name)

if __name__ == '__main__':
    do_hello()
